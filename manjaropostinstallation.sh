#!/bin/bash
echo"Script de configuration post installation de manjaro linux par Jérôme Hanoteau sous GPLv3"
echo"A utiliser sans aucune garantie juste après une installation de manajro xfce ou kde (gnome et autres moins complet)" 
# Mise à jour
# sudo pacman -Syu
# mise à jour des clés
# sudo pacman -Sy archlinux-keyring manjaro-keyring 
# sudo pacman -Syu
# sudo pacman-key --populate archlinux manjaro 
# sudo pacman -Syu
# sudo pacman-key --refresh-keys
#mise à jour d'une clé particulière
#gpg --recv-key 0FC3042E345AD05D
#sudo pacman -S vim
#sudo pacman -S yay
#yay -Syu
# suppresion du beep système très agaçant
xset -b
if [ -f "~/.xprofile" ]
then
	echo "xset -b" >> ~/.xprofile
else
	touch ~/.xprofile
	echo "xset -b" > ~/.xprofile
fi
# Francisation du système
#if locale -a == fr_BE ou fr_FR ou fr_CA
#sudo pacman -S firefox-i18n-fr aspell-fr hyphen-fr mythes-fr languagetool grammalecte-fr
#sudo yay -S libreoffice-extension-grammalecte-fr
# Gestion des imprimantes
#sudo pacman manjaro-printer cups cups-filters cups-pdf cups-pk-helper system-config-printer 
#yay -S ttf-ms-fonts
# gestion de eid si timezone Bruxelles
timezone="Europe/Brussels"
if grep -Fxq "$timezone" /etc/timezone
then
    sudo pacman -S ccid pcsc-tools
    yay -S eid-mw
    sudo systemctl start pcscd.service
    sudo systemctl enable pcscd.service
else
    echo "vous n'avez pas configuré Bruxelles comme votre position pas d'installation automatique des outils de gestion de l'eid belge voir hanoteau.blogspot.com pour installation manuelle si nécessaire"
fi
# configuration spécifique au desktop XFCE ou KDE pris en charge seulement
if ($XDG_CURRENT_DESKTOP=="XFCE")
then
	yay -S plank
	sudo pacman -S xfce4-goodies
fi
if ($XDG_CURRENT_DESKTOP=="KDE")
then
	yay -S latte-dock
fi
#installation des clients pour le could Dropbox, pcloud et google drive
yay -S dropbox pcloud-drive odrive-bin
# optimisation ssi preocesseur intel
#if cat /proc/cpuinfo vendor_id       : GenuineIntel
#sudo pacman -S intel-ucode
#pour empêcher de lancer le support bluetooth ( démarrage + rapide) créer le fichier blacklist.conf :
#sudo -H vim /etc/modprobe.d/blacklist.conf
#et ajouter dedans (insert (i)):
#install bluetooth /bin/false 
#kde seulement:
#look and feel apparence : choisir Brise
#sudo nano /etc/sysctl.d/100-manjaro.conf
#In the file, you have to put the following:
#vm.swappiness=10
#disque SSD seulement:
#sudo systemctl enable fstrim.timer
#!!!!!! configuration imprimante epson WF 4725 !!!!!! 
# yay -S epson-inkjet-printer-escpr2
#verifier la version de hplip et telecharger depuis le site si necessaire


